package br.com.ifc.it.test.support;

import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;
import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.SeleniumException;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;

/**
 * The type Selenium test support.
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public abstract class SeleniumTestSupport {

    private static final String WINDOWS_OS = "Windows";
    private static final int SELENIUM_TIMEOUT = 15;
    private static final int SELENIUM_TIME_TO_FIND = 5;
    
    public Selenium selenium;
    private String baseUrl;
    private WebDriver driver;
    private Wait<WebDriver> wait;
    
    /**
     * Init test.
     *
     * @throws Exception the exception
     */
    @Before
    public void initTest() throws Exception {
        if(System.getProperty("os.name").startsWith(WINDOWS_OS)){
            System.setProperty("webdriver.chrome.driver", URLDecoder.decode(getClass().getResource("/chromedriver.exe").getPath(), "UTF-8"));
        } else {
            System.setProperty("webdriver.chrome.driver", getClass().getResource("/chromedriver").getPath());
        }
        driver = new ChromeDriver();
        setBaseUrl();
        createWebDriver(driver);
        setUp();
        configureWait();
    }
    
    public abstract void setUp();
    
    /**
     * SET BASEURL IN THIS METHOD
     */
    public abstract void setBaseUrl();

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    @After
    public void tearDown() throws Exception {
        if(selenium != null) {
            selenium.stop();
        }
    }
    
	private void configureWait() {
        wait = new FluentWait<WebDriver>(driver)
        		.withTimeout(SELENIUM_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(SELENIUM_TIME_TO_FIND, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
	}

    /**
     * Create a {@link WebDriver} to control and iterate with browser
     * @param driver
     */
    private void createWebDriver(WebDriver driver) {
        try {
            selenium = new WebDriverBackedSelenium(driver, baseUrl);
        } catch (SeleniumException ex) {
            throw new IllegalArgumentException("In setUp method set the base URL for the test!");
        }
    }

    /**
     * Get the {@link WebDriver}               
     * @return {@link WebDriver}
     */
    public WebDriver getDriver(){
    	return this.driver;
    }
    
    /**
     * Set base url.
     *
     * @param baseUrl the base url
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * Find the first {@link WebElement} through the passed id
     * @param id to be found
     * @return {@link WebElement} 
     */
    public WebElement waitUntilById(final String id){
    	return wait.until(new Function<WebDriver, WebElement>() {

			@Override
			public WebElement apply(WebDriver arg0) {
				return driver.findElement(By.id(id));
			}
    		
		});
    }
    
    /**
     * Find the first {@link WebElement} through the passed css class name
     * @param className to be found
     * @return {@link WebElement} 
     */
    public WebElement waitUntilByClass(final String className){
    	return wait.until(new Function<WebDriver, WebElement>() {

			@Override
			public WebElement apply(WebDriver arg0) {
				return driver.findElement(By.className(className));
			}
    		
		});    	
    }

}
