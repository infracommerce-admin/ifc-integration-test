package br.com.ifc.it.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//TODO: verificar testes da Toriba e JJ

/**
 * The type Cart tests suite.
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	br.com.ifc.it.test.cart.lp.FreightTest.class, 
	br.com.ifc.it.test.cart.ogio.FreightTest.class, 
	br.com.ifc.it.test.cart.ecko.FreightTest.class, 
	br.com.ifc.it.test.cart.flg.FreightTest.class, 
	br.com.ifc.it.test.cart.mzct.FreightTest.class, 
	br.com.ifc.it.test.cart.rednose.FreightTest.class, 
	br.com.ifc.it.test.cart.jj.FreightTest.class, 
	br.com.ifc.it.test.cart.toriba.FreightTest.class
	})
public class CartTestsSuite {
}
