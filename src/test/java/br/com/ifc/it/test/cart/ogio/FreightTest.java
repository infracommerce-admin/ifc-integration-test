package br.com.ifc.it.test.cart.ogio;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.com.ifc.it.test.cart.FreightTestInterface;
import br.com.ifc.it.test.support.SeleniumTestSupport;

public class FreightTest extends SeleniumTestSupport implements FreightTestInterface {

	private final String SKU = "97664";
	
	@Override
	public void setBaseUrl() {
		setBaseUrl("http://cart.ogio.dev.accurate.com.br/ckout/");
	}
	
	@Override
	public void setUp() {
		addProductToCart();
	}
	
	@Override
	@Test
	public void testFreightCalculatedCorrectly() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=tr.product"));
		
		selenium.click("css=a.frete");
		selenium.type("id=cepValue1", "01010");
		selenium.type("id=cepValue2", "010");
		selenium.click("id=calculafrete");

		assertTrue("A lista de frete dispon�vel para o cep informado n�o foi exibida", waitUntilById("listCorreiosFreightTable") != null);
		
	}
	
	@Override
	@Test
	public void testCarrierNotAvailableAtZipCode() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=tr.product"));
		
		selenium.click("css=a.frete");
		selenium.type("id=cepValue1", "99999");
		selenium.type("id=cepValue2", "999");
		selenium.click("id=calculafrete");
		
		assertTrue("O popup de transportadora n�o encontrada n�o foi exibido", waitUntilById("cboxClose") != null);
		
	}
	
	@Override
	@Test
	public void testZipCodeNotFound() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=tr.product"));
		
		selenium.click("css=a.frete");
		selenium.type("id=cepValue1", "00000");
		selenium.type("id=cepValue2", "000");
		selenium.click("id=calculafrete");
		
		assertTrue("O popup de transportadora n�o encontrada n�o foi exibido", waitUntilById("cboxClose") != null);
		
	}

	private void addProductToCart(){
		selenium.open("/ckout/addItems.xhtml?addsku={" + SKU + ",1,}");
	}

}
