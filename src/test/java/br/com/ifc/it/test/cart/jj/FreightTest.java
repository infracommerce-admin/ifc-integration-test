package br.com.ifc.it.test.cart.jj;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.Test;
import org.openqa.selenium.Cookie;

import br.com.ifc.it.test.cart.FreightTestInterface;
import br.com.ifc.it.test.support.SeleniumTestSupport;

public class FreightTest extends SeleniumTestSupport implements FreightTestInterface {

	private final String SKU = "7909101389317";
	
	@Override
	public void setBaseUrl() {
		setBaseUrl("http://cart.jj.dev.accurate.com.br/ckout/");
	}
	
	@Override
	public void setUp() {
		createAffiliateCookie();
		this.setBaseUrl();
		addProductToCart();
	}
	
	@Override
	@Test
	public void testFreightCalculatedCorrectly() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=.product"));
		
		selenium.type("id=cepValue1", "01010");
		selenium.type("id=cepValue2", "010");
		selenium.click("id=calculafrete");

		assertTrue("A lista de frete dispon�vel para o cep informado n�o foi exibida", waitUntilById("listCorreiosFreightTable") != null);
		
	}
	
	@Override
	@Test
	public void testCarrierNotAvailableAtZipCode() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=.product"));
		
		selenium.type("id=cepValue1", "99999");
		selenium.type("id=cepValue2", "999");
		selenium.click("id=calculafrete");
		
		assertTrue("O popup de transportadora n�o encontrada n�o foi exibido", waitUntilById("cboxClose") != null);
		
	}
	
	@Override
	@Test
	public void testZipCodeNotFound() throws Exception {
		assertTrue("Nenhum produto adicionado ao carrinho", selenium.isElementPresent("css=.product"));
		
		selenium.type("id=cepValue1", "00000");
		selenium.type("id=cepValue2", "000");
		selenium.click("id=calculafrete");
		
		assertTrue("O popup de transportadora n�o encontrada n�o foi exibido", waitUntilById("cboxClose") != null);
		
	}
	
	private void addProductToCart(){
		selenium.open("/ckout/addItems.xhtml?addsku={" + SKU + ",1,}");
	}

	private void createAffiliateCookie() {
		selenium.open("/");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, +1);
		getDriver().manage().addCookie(new Cookie("AFFILIATE_MAPPING", "", ".jj.dev.accurate.com.br", "/", c.getTime()));
	}

}
