package br.com.ifc.it.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The type Sserv tests suite.
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({})
public class SservTestsSuite {
}
