package br.com.ifc.it.test.cart;

import org.junit.Test;

public interface FreightTestInterface {

	/**
	 * Test to make sure that the freight is calculated correctly when a correct zipcode is entered 
	 * @throws Exception
	 */
	@Test
	void testFreightCalculatedCorrectly() throws Exception;
	
	/**
	 * Test to verify if the system show a message for client when none carrier is found for the zipcode entered 
	 * @throws Exception
	 */
	@Test
	public void testCarrierNotAvailableAtZipCode() throws Exception;
	
	/**
	 * Test to verify if the system show a message for client when the zipcode entered  was not found
	 * @throws Exception
	 */
	@Test
	public void testZipCodeNotFound() throws Exception;
	
}
