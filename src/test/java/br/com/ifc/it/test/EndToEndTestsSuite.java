package br.com.ifc.it.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The type End to end tests suite.
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({})
public class EndToEndTestsSuite {
}